import React, { Component } from 'react';
import _ from 'underscore';
import recipes from '../recipes.json';
import localStorage from './lib/store.js';
import utility from './utility/utility.js';

// Initializations
const { findIndexByName } = utility;
const RECIPE_SELECTIONS = 'RECIPE_SELECTIONS';

// Components
import RecipeItem from './RecipeItem.js';

class Recipes extends Component {
  constructor(props){
    super(props);
    const lsSelections = localStorage.get(RECIPE_SELECTIONS);
    const selections = (Array.isArray(lsSelections)) ? lsSelections : [];
    this.state = {
      selections,
      searchFilter: ''
    }
    this.handleFilterSearch = this.handleFilterSearch.bind(this);
    this.handleRecipeSelection = this.handleRecipeSelection.bind(this);
    this.addOrReturnSelections = this.addOrReturnSelections.bind(this);
  }

  /**
  * Local storage handler of selections
  */
  persistSelections(selections){
    localStorage.set(RECIPE_SELECTIONS, selections);
  }

  /**
   * handles a text based filtering
   * @param evt {Object} - event to extract value
   */
  handleFilterSearch(evt) {
    const searchFilter = evt.target.value;
    this.setState({ searchFilter });
  }

  /**
   * Handles a selection of a recipe item by caching and local persisting
   * @param selection {Object} - recipe object
   * @param shouldSelect {Boolean} - add/remove
   */
  handleRecipeSelection(selection, shouldSelect) {
    const selections = shouldSelect ?
      this.addOrReturnSelections(selection) :
      this.removeOrReturnSelections(selection);

    this.setState({ selections });
    this.persistSelections(selections);
  }

  /**
   * Determines whether to add to a recipe list to avoid duplicates
   * @param selection {Object} - recipe selected
   */
  addOrReturnSelections(selection) {
    const { selections } = this.state;
    const index = findIndexByName(selections, selection.name);
    if (index < 0){
      selections.push(selection);
    }

    return selections;
  }

  /**
   * Determines whether to extract a recipe from a recipe list
   * @param selection {Object} - recipe unselected
   */
  removeOrReturnSelections(selection) {
    const { selections } = this.state;
    const index = findIndexByName(selections, selection.name);

    return [
      ...selections.slice(0, index),
      ...selections.slice(index+1, selections.length)
    ];
  }

  /**
   * Filters the recipes by using string based ingredient search
   * @param searchFilter {String} - search text
   */
  filterRecipes(searchFilter) {
    return recipes.filter(recipe => {
      const ingredientString = recipe.ingredients.join('*').toLowerCase();
      return ingredientString.indexOf(searchFilter.toLowerCase()) > -1;
    });
  }

  /**
   * Finds the non-intersection/distinct ingredients based on
   * @param recipes {Array} - recipe list
   */
  determineDistinctRecipeIngredients(recipes) {
    console.log('recipes', recipes);
    const distinct = [];
    const seen = {};
    recipes.forEach((recipe)=>{
      recipe.ingredients.forEach((ingredient) => {
        seen[ingredient] = seen[ingredient] ? -1 : 1;
      });
    });

    for (const ingredient in seen){
      if (seen[ingredient] === 1){distinct.push(ingredient)}
    }

    return distinct;
  }

  /**
   * Comma seperates (Oxfordian)
   * @param {Array} strArr - list of strings
   */
  commaSeperateStrings(strArr) {
    const duplicatedArr = strArr.slice(0);
    if (strArr.length > 2){
      const last = duplicatedArr.pop();
      return duplicatedArr.join(', ') + ', and ' + last;
    }
    if (strArr.length === 2){
      return duplicatedArr.join(' and ');
    }
    if (strArr.length === 1){
      return strArr[0];
    }
  }

  render() {
    const { searchFilter, selections } = this.state;
    const filteredRecipes = this.filterRecipes(searchFilter) || [];
    const distinctIngredients = this.determineDistinctRecipeIngredients(selections).sort();

    return (
      <div className="Recipes">
        <div className='sub-nav'>
          <input className='search' placeholder='Ingredient Search' onChange={this.handleFilterSearch} value={this.state.searchFilter}/>
        </div>
        {filteredRecipes.length > 0 ?
          <section className='RecipeTable--wrapper'>
            { filteredRecipes.map((recipe) => {
              return (
                <RecipeItem
                key={`recipe-${recipe.name}`}
                isSelected={findIndexByName(selections, recipe.name) > -1}
                recipe={recipe}
                onSelection={this.handleRecipeSelection}
                />
              );
            })}
            { distinctIngredients.length > 0 ?
              <div className='distinct-ingredients'>
              <h3 className='bold'>These are the distinct ingredients from ingredients selected</h3>
              {this.commaSeperateStrings(distinctIngredients)}
              </div> : null
            }
          </section>
          :
          <div>
            The ingredient <span className='bold'>"{searchFilter}"</span> is not in your recipes
          </div>
        }
      </div>
    );
  }
}

export default Recipes;
