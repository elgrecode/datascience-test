const _ = require('underscore');
const utility = {
  findIndexByName: (arr, name) => {
    const baseIndex = -1;
    const len = arr.length;
    for (let i = 0; i < len; i++) {
      if (arr[i].name === name){ return i; }
    }

    return baseIndex;
  }
}

export default utility
