import React, { Component } from 'react';
import './DataScience.css';
import logo from './logo.svg';
import Recipes from './Recipes.js';

class DataScience extends Component {
  render() {
    return (
      <div className="DataScience">
        <div className="DataScience-header">
          <img src={logo} className="DataScience-logo" alt="logo" />
        </div>
        <div className="DataScience-content">
          <Recipes />
        </div>
      </div>
    );
  }
}

export default DataScience;
