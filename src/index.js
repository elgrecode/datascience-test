import React from 'react';
import ReactDOM from 'react-dom';
import DataScience from './DataScience';
import './index.css';

ReactDOM.render(
  <DataScience />,
  document.getElementById('root')
);
