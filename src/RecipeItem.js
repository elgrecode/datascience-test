import React, { Component } from 'react';
import './css/checkbox.css';

class RecipeItem extends Component {
  constructor(props){
    super(props);
    this.state = {
      isSelected: false
    }
    this.onSelection = this.onSelection.bind(this);
  }

  /**
   * Handles selection on a particular recipe item
   */
  onSelection(){
    const { onSelection, recipe, isSelected } = this.props;
    if (typeof onSelection === 'function'){
      onSelection(recipe, !isSelected);
    }
  }

  renderIngredientPills(ingredients){
    return ingredients.map(i => {
      return (
        <span className='pill'>{i}</span>
      );
    })
  }

  render() {
    const { isSelected, recipe } = this.props;
    const { name, ingredients, type, cook_time } = recipe;

    return (
      <div key={`recipe-${name}`} className={`RecipeItem flex ${isSelected ? 'is-selected' : ''}`}>
      <div className='check-box-wrapper'>
        <div className="checkbox">
          <input type="checkbox" id={`recipe-${name}`} checked={isSelected} onChange={this.onSelection}/>
          <label htmlFor={`recipe-${name}`}> </label>
        </div>
      </div>
        <div className='flex1 cell'>{name}</div>
        <div className='flex3 cell'>{this.renderIngredientPills(ingredients)}</div>
        <div className='flex1 cell'>{type}</div>
        <div className='flex1 cell'>{cook_time} minutes</div>
      </div>
    );
  }
};

export default RecipeItem;
