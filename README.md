DataScience's Frontend Code Test
==================

## Instructions to Critique
* Download or clone this repo `git clone https://gitlab.com/elgrecode/datascience-test.git`
* `cd datascience-test` to get in main directory
* `npm install` in the main directory (use node version 4+, preferably v6.1.0)
* `npm run start` to build and have webpack watch changes if you'd like to mess around with the code (localhost:3000 or other port)
* Look longingly into the distance as you question this app's greatness
* Prepare yourself a dish with your favorite recipes

This is an at-home exercise that we use as part of our standard interview process for frontend and full-stack developers.

## Instructions

* Clone this repo
* Complete the exercise and submit either a zip of the solution or a link to a new repo
* You may use any resources, frameworks, libraries, etc and style the markup however you like.

## Requirements

Using the provided JSON data representing a collection of meal recipes, create a micro frontend application that meets the following criteria:

* Display a list (or table) of recipes.
* Allow filtering of recipes by a single ingredient.
* Add checkboxes to allow selection of multiple recipes.
* Show an alphabetically ordered list of distinct ingredients for the selected recipes. This should update as recipes are selected / unselected.
* Persist the selections locally and regenerate the view on page refresh.
* In a README note any required setup to be able to run the app, such as modifying hosts file, etc.
